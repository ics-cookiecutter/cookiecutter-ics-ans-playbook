Cookiecutter Ansible Playbook
=============================

Cookiecutter_ template for ESS ICS Ansible_ playbook: ics-ans-<name>

Quickstart
----------

Install the latest Cookiecutter if you haven't installed it yet::

    $ pip install cookiecutter

Generate an Ansible playbook project::

    $ cookiecutter git+https://gitlab.esss.lu.se/ics-cookiecutter/cookiecutter-ics-ans-playbook.git

As this is not easy to remember, you can add an alias in your *~/.bash_profile*::

    alias new-playbook='cookiecutter git+https://gitlab.esss.lu.se/ics-cookiecutter/cookiecutter-ics-ans-playbook.git'

Detailed instructions
---------------------

The playbook will be named `ics-ans-<name>`. `ics-ans-` is automatically prepended.
Only enter `<name>` when asked for the playbook_name.

To create `ics-ans-foo`::

    $ cookiecutter git+https://gitlab.esss.lu.se/ics-cookiecutter/cookiecutter-ics-ans-playbook.git
    company [European Spallation Source ERIC]:
    full_name [Benjamin Bertrand]:
    playbook_name [ics-ans-<name>]: my-playbook
    playbook_group [my_playbook]:
    description [Ansible playbook to install my-playbook]:
    Select molecule_driver:
    1 - docker centos:7
    2 - docker centos:7 with systemd
    3 - vagrant centos/7
    4 - docker ubuntu:22.04
    5 - docker ubuntu:22.04 with systemd
    6 - vagrant ubuntu/jammy64
    7 - vagrant juniper/vqfx10k-re
    Choose from 1, 2, 3, 4, 5, 6, 7 [1]:

This creates the following project::

    ics-ans-my-playbook/
    ├── LICENSE
    ├── README.md
    ├── group_vars
    │   └── my_playbook
    ├── molecule
    │   └── default
    │       ├── molecule.yml
    │       ├── prepare.yml
    │       └── tests
    │           └── test_default.py
    ├── playbook.yml
    └── roles
        └── requirements.yml

The project includes required files to test the role using Molecule_.

License
-------

BSD 2-clause license

.. _Ansible: http://docs.ansible.com/ansible/
.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _Molecule: http://molecule.readthedocs.io/en/master/
